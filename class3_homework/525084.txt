I've learned a lot of new things in this course.

I enjoy the classed, your presentation skills are great. The slides are also great, it's good to have the commands written out in case we forget and we can just check the slides. The fact that we don't do much git practical stuff at the seminars is fine for me, we have a lot of materials to get through in 6 weeks and we can try out the practical stuff at home.

I've learned a lot about differences between upstream and origin, the different actions one can do between them, I've learned to not use checkout anymore.
